# ChefDK Docker Environment

This project build a Docker environment to run ChefDK for Chef development.

## Build

To build the Docker image:
```
./build.sh
```

## Run

To run the Docker image:
```
./run.sh
```

That's it!
