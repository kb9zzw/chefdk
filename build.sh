#!/bin/bash

TAG=$1

if [ -z "$TAG" ]; then
  TAG=chefdk
fi

docker build -t $TAG --build-arg USER=$USER --build-arg UID=$UID .
