#!/bin/bash

TAG=$1

if [ -z "$TAG" ]; then
  TAG=chefdk
fi

docker run --rm -it -v $HOME:/home/$USER:z -w /home/$USER --user $USER --hostname $TAG.docker $TAG
