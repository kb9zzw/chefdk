FROM docker.io/centos:latest

# container user account
ARG USER=testuser
ARG UID=1000

# Load chefdk rpm
COPY files/chefdk-4.3.13-1.el7.x86_64.rpm /tmp/chefdk-4.3.13-1.el7.x86_64.rpm

# Install a bunch of stuff
RUN yum install -y \
  git \
  vim \
  which \
  curl \
  sudo \
  /tmp/chefdk-4.3.13-1.el7.x86_64.rpm && \
  yum update -y; yum clean all

# Cleanup
RUN rm -rf /tmp/chefdk-4.3.13-1.el7.x86_64.rpm

# Local user with admin privs
RUN echo "$USER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
RUN useradd $USER -u $UID
